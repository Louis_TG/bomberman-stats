#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#--------------------------------
#
# Author            : Lasercata
# Last modification : 2023.12.19
# Version           : v4.1.0
#
#--------------------------------
version = 'v4.1.0'


'''This script tries the bomberman AI and show stats about it.'''

# Note :
#   As the game motor certainly uses `srand(time(NULL))` to initialize the random number generator,
#   if your program is deterministic, then if you run it multiple times in the same second, you will
#   obtain the same result.
#
#   In order to avoid this, this script uses gdb to change the seed in the runtime.


##-Imports
# from os import popen
# from os import walk
from subprocess import Popen, PIPE, TimeoutExpired

from threading import Thread, Event
from os import sched_getaffinity # to get the number of cores
from os.path import isfile

from datetime import datetime as dt
from time import sleep

import argparse
from sys import argv #to have the name of the program in the examples
from sys import exit as sysexit

from random import randint

try:
    from tqdm import tqdm
    has_pb = True

except ImportError:
    has_pb = False
    print("Warning: you don't have `tqdm` (used to show a nice progress bar). Install it with `pip install tqdm`.")


##-Ini
game_over_part = '\\____/\\__,_|_| |_| |_|\\___|  \\___/  \\_/ \\___|_|'


##-Utils
def basename(path):
    return path.split('/')[-1]


##-Functions
def exec_prog(exec_name, level_name, seed=None, debug=False):
    '''
    Run the command using Popen, and return the pipe object.

    - exec_name  : the name of the executable ;
    - level_name : the name of the level to choose ;
    - seed       : If None, run the program normally. Otherwise, it should be
                   an int, which will be used as the seed for random in the
                   program (using gdb to change the seed in srand).
    '''

    if debug:
        b_command_line = f'{exec_name} -delay 0 -debug on -display bw {level_name}'
    else:
        b_command_line = f'{exec_name} -delay 0 -display bw {level_name}'

    if seed == None:
        command_line = b_command_line

    elif type(seed) != int:
        raise ValueError('exec_prog: seed argument should be an int !')

    else:
        command_line = f'echo -e "set confirm on\\nb srand\\nr\\ncall x = {seed}\\nc\\nq\\n" | gdb --args {exec_name} -delay 0 -debug on -display bw {level_name}'

        # command_line = f'''gdb \\
        #     -nx --batch \\
        #     -ex "b srand" \\
        #     -ex r \\
        #     -ex "call x = {seed}" \\
        #     -ex c \\
        #     -ex q \\
        #     --args '''
        # command_line += b_command_line


    # print(debug)
    # print(command_line)
    # print(seed)
    #
    return Popen(command_line, stdout = PIPE, stderr = PIPE, shell=True)


def get_res_string(exec_name, level_name, timeout=3000, use_gdb=True, seed=None, debug=False, verbose=False):
    '''
    Calculate and return the success, the score, and the number of steps of the AI 'exec_name' for a game.

    Return : (success, score, nb_steps, seed)

    `success` is -1 if the execution crashed (with a segmentation fault for example).
    `success` is -2 if the execution timed out.

    - exec_name  : the name of the executable ;
    - level_name : the name of the level to choose ;
    - timeout    : the time to wait before killing the process (in ms). Default is 3s ;
    - use_gdb    : if True, uses gdb to change the seed ;
    - seed       : if not None, use this seed (and ignore the previous argument) ;
    - verbose    : if True, show the board at the end of each try, along with (success, score)
    '''

    if seed != None:
        pipe = exec_prog(exec_name, level_name, seed=seed, debug=debug)

    elif use_gdb:
        seed = randint(0, int(10e8))
        pipe = exec_prog(exec_name, level_name, seed=seed, debug=debug)
    
    else:
        seed = int(dt.now().timestamp())
        pipe = exec_prog(exec_name, level_name, seed=None, debug=debug)

    try:
        res_s, err = pipe.communicate(timeout = timeout / 1000)

        string = res_s.replace(b'\x1b[14A', b'').replace(b'\x00', b'')
        string = string.decode(errors='replace')

        res_a = string.split('\n')[:-1]

        # Get the result of the AI (success : True, or fail : False)
        if use_gdb:
            success = game_over_part not in res_a[-2]
        else:
            success = game_over_part not in res_a[-1]

        if err != b'' and b'random.c: ' not in err and b'warning:' not in err:
            success = -1

            if verbose:
                print(f'The error for this execution : {err.decode()}')

    except TimeoutExpired:
        success = -2
        if verbose:
            print(f'Execution timed out after {timeout / 1000}s')

    # Get the score
    score = -1
    score_line_nb = -1
    nb_steps = -1

    if success not in (-2, -1):
        for k, line in enumerate(reversed(res_a)):
            if 'score' in line.lower():
                score = int(line.split(' ')[1])
                score_line_nb = k
                break

        if score == -1:
            raise ValueError('The output is not correctly formatted !')

        # Get the number of steps
        nb_steps = string.count('SCORE')
        # nb_steps = string.count('Bomberman action is:')

        if verbose:
            if debug:
                print(string)
            else:
                print('\n'.join(res_a[l] for l in range(-score_line_nb - 1, 0)))

            print(f'success : {success}, score : {score}, steps : {nb_steps}, seed : {seed}\n')

    pipe.terminate() # This allow to free the memory of the launched program.

    return (success, score, nb_steps, seed)


def test_sample(exec_name, level_name, nb, timeout=3000, wait=0, use_gdb=True, use_pb=True, verbose=False, run_event=None):
    '''
    Run the program `exec_name` `nb` times on the level `level_name`, and return the results, in the form of four arrays (lists in python ...) and an int : (success, scores, steps, seeds, nb_success).

    success[k] : a bool indicating if the k-th execution resulted in a win for the AI ;
    scores[k]  : the score obtained at the k-th execution ;
    steps[k]   : the number of steps done at the k-th execution ;
    seeds[k]   : the seed used for the k-th execution ;
    nb_success : the number of success.

    - exec_name  : the name of the executable ;
    - level_name : the name of the level to choose ;
    - nb         : the number of times to run the program ;
    - timeout    : the time to wait before killing the process (in ms). Default is 3s ;
    - wait       : how many time sleep between each run (in milliseconds). Default is 0 ;
    - use_gdb    : if True, uses gdb to change the seed ;
    - use_pb     : if False, don't uses the tqdm progress bar ;
    - verbose    : if True, show the board at the end of each try, along with (success, score) ;
    - run_event  : TODO !!!!
    '''

    # Initiate variables
    scores = []
    success = []
    steps = []
    seeds = []
    nb_success = 0

    # Run the program and get values
    if has_pb and use_pb:
        rg = tqdm(range(nb))
    else:
        rg = range(nb)

    for k in rg:
        try:
            try:
                has_win, score, nb_steps, seed = get_res_string(exec_name, level_name, timeout=timeout, use_gdb=use_gdb, verbose=verbose)

                if run_event != None and not run_event.is_set():
                    raise KeyboardInterrupt
            
            except KeyboardInterrupt:
                print('\nStopped.')
                return (success, scores, steps, seeds, nb_success)

            scores.append(score)
            success.append(has_win)
            steps.append(nb_steps)
            seeds.append(seed)

            if has_win == True: # the '== True' is needed as `has_win` can be -1.
                nb_success += 1

        except ValueError as err:
            if verbose:
                print(err)

            pass

        sleep(wait / 1000)

    return (success, scores, steps, seeds, nb_success)


def show_sample_stats(exec_name, level_name, nb, timeout=3000, wait=0, use_gdb=True, use_pb=True, verbose=False):
    '''
    Use the `test_sample` function and calculate stats from it.

    - exec_name  : the name of the executable ;
    - level_name : the name of the level to choose ;
    - nb         : the number of times to run the program ;
    - timeout    : the time to wait before killing the process (in ms). Default is 3s ;
    - wait       : how many time sleep between each run (in milliseconds). Default is 0 ;
    - use_gdb    : if True, uses gdb to change the seed ;
    - use_pb     : if False, don't uses the tqdm progress bar ;
    - verbose    : if True, show the board at the end of each try, along with (success, score)
    '''

    t0 = dt.now()
    (success, scores, steps, seeds, nb_success) = test_sample(exec_name, level_name, nb, timeout, wait, use_gdb, use_pb, verbose)
    t1 = dt.now()

    print_stats(exec_name, level_name, success, scores, steps, seeds, nb_success, t1 - t0)


def print_stats(exec_name, level_name, success, scores, steps, seeds, nb_success, t):
    '''
    Prints the result for a list of executions.

    - exec_name  : the name of the executable ;
    - level_name : the name of the level to choose ;
    - success    : the bool list of successes ;
    - scores     : the list of scores ;
    - steps      : the list of steps ;
    - seeds      : the list of seeds ;
    - nb_success : the number of successes ;
    - t          : the time elapsed.
    '''

    nb = len(success) # Change this if the user stopped it with KeyboardInterrupt.

    if nb == 0:
        print('Nothing to show (stopped before an execution finished).')
        return

    situations = [(success[k], scores[k], steps[k], seeds[k]) for k in range(nb)]
    situations = list(set(situations)) # Remove doubles

    nb_different_sit = len(situations)

    #------Calculate stats
    scores_success = []
    steps_success = []
    for k in range(len(scores)):
        if success[k] == True: # the '== True' is needed as `success[k]` can be -1.
            scores_success.append(scores[k])
            steps_success.append(steps[k])

    win_rate = round(100 * nb_success / nb, 2)
    nb_success_diff = [k[0] for k in situations].count(True)
    win_rate_diff = round(100 * nb_success_diff / nb_different_sit, 2)

    # print(nb_success)

    #------Print stats :
    print(f'Stats for {nb} executions of "{basename(exec_name)}" on the level "{basename(level_name)}" :\n')

    #---Steps
    #-All
    print('Steps :')
    print(f'\tMaximum number of steps : {max(steps)}')
    print(f'\tMinimum number of steps : {min(steps)}')
    print(f'\tAverage number of steps : {round(sum(steps) / len(steps), 2)}')

    #-Success
    if nb_success != 0 and nb_success < nb:
        print(f'\n\tMaximum number of steps when won : {max(steps_success)}')
        print(f'\tMinimum number of steps when won : {min(steps_success)}')
        print(f'\tAverage number of steps when won : {round(sum(steps_success) / len(steps_success), 2)}')

    #---Score
    #-All
    print('\nScore :')
    print(f'\tMaximum score : {max(scores)}')
    print(f'\tMinimum score : {min(scores)}')
    print(f'\tAverage score : {round(sum(scores) / len(scores), 2)}')

    #-Success
    if nb_success != 0 and nb_success < nb:
        print(f'\n\tMaximum score when won : {max(scores_success)}')
        print(f'\tMinimum score when won : {min(scores_success)}')
        print(f'\tAverage score when won : {round(sum(scores_success) / len(scores_success), 2)}')

    #---Different execution
    if nb_different_sit != nb:
        print(f'\nNumber of really different executions : {nb_different_sit}.')

    else:
        print()

    #---Crashes, timeout
    craches_nb = success.count(-1)
    print(f'Number of crashes : {craches_nb} ({round(100 * craches_nb / nb, 2)}%).')
    timeout_nb = success.count(-2)
    print(f'Number of timeout : {timeout_nb} ({round(100 * timeout_nb / nb, 2)}%).')

    #---Rate
    print(f'Win rate : {win_rate}% ({nb_success} won on {nb} games).')

    if nb_different_sit != nb:
        print(f'Win rate (for different executions) : {win_rate_diff}% ({nb_success_diff} won on {nb_different_sit} games).')

    #---Time
    print(f'\n{t}s elapsed.')


def show_seed_stat(exec_name, level_name, seed, timeout=3000, verbose=False):
    '''
    Show the stats for one execution with a given seed.

    - exec_name  : the name of the executable ;
    - level_name : the name of the level to choose ;
    - seed       : the random seed ;
    - timeout    : the time to wait before killing the process (in ms). Default is 3s ;
    - verbose    : indicating if being verbose.
    '''

    success, score, nb_steps, seed_ = get_res_string(exec_name, level_name, timeout=timeout, seed=seed, debug=True, verbose=verbose)

    print(f'Stats for the execution of "{exec_name}" on the level "{level_name}" with the seed "{seed}" :\n')

    if success == -1:
        print('The program crashed.')
        return

    print(f'\tSuccess  : {success}')
    print(f'\tScore    : {score}')
    print(f'\tNb_steps : {nb_steps}')


##-Threading
def get_nb_available_threads():
    '''Returns the number of available threads'''

    return len(sched_getaffinity(0))

def test_sample_thread(func, args, res_l, index):
    '''
    This is the function ran by threads. It is needed as we need to get the results.
    Run `res_l[index] = func(*args)`.

    - func  : the function to call ;
    - args  : the arguments of the function ;
    - res_l : the list that will contain the result ;
    - index : the index of `res_l` at which write the result. `len(res_l)` should be at least of index + 1.
    '''

    res_l[index] = func(*args)


def launch_multithreads(exec_name, level_name, nb, timeout=3000, wait=0, use_gdb=True, use_pb=True, verbose=False, nb_threads=None):
    '''
    Launch a sample test on multiple threads.

    - exec_name  : the name of the executable ;
    - level_name : the name of the level to choose ;
    - nb         : the number of times to run the program ;
    - timeout    : the time to wait before killing the process (in ms). Default is 3s ;
    - wait       : how many time sleep between each run (in milliseconds). Default is 0 ;
    - use_gdb    : if True, uses gdb to change the seed ;
    - verbose    : if True, show the board at the end of each try, along with (success, score) ;
    - use_pb     : if False, don't uses the tqdm progress bar ;
    - nb_threads : the number of threads to use. If None (default), will be set to the number of available cores.
    '''

    #Number of threads
    if nb_threads == None:
        nb_threads = get_nb_available_threads()

    #Calculating the number of executions per thread
    nb_iter_lst = [nb // nb_threads] * nb_threads

    for k in range(nb % nb_threads):
        nb_iter_lst[k] += 1

    # assert(sum(nb_iter_lst) == nb)

    #Initialisation of the threads
    res_l = [0] * nb_threads
    run_event = Event()
    run_event.set()

    th = [
        Thread(
            target=test_sample_thread,
            args=(
                test_sample,
                (exec_name, level_name, nb_iter_lst[k], timeout, wait, use_gdb, use_pb, verbose, run_event),
                res_l,
                k
            )
        )
        for k in range(nb_threads)
    ]

    # Starting the threads
    t0 = dt.now()
    for t in th:
        t.start()

    for t in th:
        try:
            t.join()

        except KeyboardInterrupt:
            print('\n\nStopping threads .....\n')
            run_event.clear()
            # sleep(.5)

    t1 = dt.now()

    old_res_l = list(res_l)
    res_l = []
    for k in old_res_l:
        if k != 0:
            res_l.append(k)

    #Concatenating results
    success = []
    for s_lst in res_l:
        success += s_lst[0]

    scores = []
    for s_lst in res_l:
        scores += s_lst[1]

    steps = []
    for s_lst in res_l:
        steps += s_lst[2]

    seeds = []
    for s_lst in res_l:
        seeds += s_lst[3]

    nb_success = sum(k[-1] for k in res_l)

    #Printing results
    if use_pb:
        print('\n' * nb_threads)

    print_stats(exec_name, level_name, success, scores, steps, seeds, nb_success, t1 - t0)


##-Parser
class Parser:
    '''Class which allow to use this script from command-line.'''

    def __init__(self):
        '''Initiate Parser'''

        self.parser = argparse.ArgumentParser(
            prog = argv[0],
            description='Show statistics about your bomberman AI',
            epilog=f'Examples :\n\t{argv[0]} ./bomberman level0.map\n\t{argv[0]} ./bomberman level0.map -n 5000\n\t{argv[0]} ./bomberman level2.map -n 60 -G -d 1000',
            formatter_class=argparse.RawDescriptionHelpFormatter
        )

        self.parser.add_argument(
            'exec',
            help='The executable name'
        )

        self.parser.add_argument(
            'level',
            help='The level file name'
        )

        self.parser.add_argument(
            '-v', '--version',
            help='Show version and exit',
            nargs=0,
            action=self.Version
        )

        self.parser.add_argument(
            '-n', '--number',
            type=int,
            help='The number of samples to use. Default is 100'
        )

        self.parser.add_argument(
            '-t', '--timeout',
            type=int,
            help='The time to wait before killing the program (in ms). Default is 3s'
        )

        self.parser.add_argument(
            '-d', '--delay',
            type=int,
            help='Add a delay (in milliseconds) between each execution (deprecated). Only useful with `-G`'
        )

        self.parser.add_argument(
            '-s', '--seed',
            type=int,
            help='If given, run the program only once with that seed for random'
        )

        self.parser.add_argument(
            '-G', '--no-gdb',
            help='Do not use gdb to change the seed',
            action='store_true'
        )

        self.parser.add_argument(
            '-P', '--no-progress-bar',
            help='Do not use the progress bar',
            action='store_true'
        )

        self.parser.add_argument(
            '-M', '--no-multi-threading',
            help='Do not use multi threading',
            action='store_true'
        )

        self.parser.add_argument(
            '-T', '--nb-threads',
            type=int,
            help=f'The number of threads to use. Default is the number of available cores ({get_nb_available_threads()} on this machine). This argument is ignored of -M is used'
        )

        self.parser.add_argument(
            '-V', '--verbose',
            help='Show the board of each game',
            action='store_true'
        )


    def parse(self):
        '''Parse the arguments'''

        #------Get arguments
        args = self.parser.parse_args()

        #number
        if args.number == None:
            nb = 100

        else:
            if args.number <= 0:
                raise argparse.ArgumentTypeError('the flag `-n` takes a strictly positive int !')

            nb = args.number

        #Delay
        if args.delay == None:
            wait = 0

        else:
            if args.delay <= 0:
                raise argparse.ArgumentTypeError('the flag `-d` takes a strictly positive int !')

            if not args.no_gdb:
                print('Warning : you do not have disabled the use of gdb to change the seed (-G), so the use of -d is useless and will be ignored.')
                wait = 0

            else:
                wait = args.delay

        #timeout
        if args.timeout == None:
            timeout = 3000

        else:
            if args.timeout <= 0:
                raise argparse.ArgumentTypeError('the flag `-t` takes a strictly positive int !')

            timeout = args.timeout

        #------Seed
        if args.seed != None:
            show_seed_stat(args.exec, args.level, seed=args.seed, timeout=timeout, verbose=args.verbose)
            sysexit()

        if args.no_multi_threading and args.nb_threads != None:
            print('get_stats: Warning: flag `-T` will be ignored as `-M` is used !')

        #------Check that file exists TODO
        if not isfile(args.exec):
            raise argparse.ArgumentTypeError(f'The file "{args.exec}" does not exists !')

        # if not isfile(args.level):
        #     raise argparse.ArgumentTypeError(f'The file "{args.level}" does not exists !')

        #------Show stats
        if args.no_multi_threading:
            show_sample_stats(args.exec, args.level, nb, timeout=timeout, wait=wait, use_gdb=(not args.no_gdb), use_pb=(not args.no_progress_bar), verbose=args.verbose)

        else:
            launch_multithreads(args.exec, args.level, nb, timeout=timeout, wait=wait, use_gdb=(not args.no_gdb), use_pb=(not args.no_progress_bar), verbose=args.verbose, nb_threads=args.nb_threads)


    class Version(argparse.Action):
        '''Class used to show version.'''

        def __call__(self, parser, namespace, values, option_string):

            print(f'get_bomberman_stats {version}')
            parser.exit()



##-Run
if __name__ == '__main__':
    app = Parser()
    app.parse()

