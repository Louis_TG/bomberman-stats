# Bomberman Stats

## Description
Ce script vous permet de tester votre IA pour `bomberman` et d'obtenir des statistiques dessus.

### Fonctionnalités :
- Affichage simple des statistiques (options par défaut) ;
- Affichage de chaque résultat de partie avec la graine (option `-V`) ;
- Affichage du déroulement d'une partie (mode débug) avec une graine donnée (options `-V -s [graine]`) ;
- Délai d'expiration pour les exécutions (option `-t [temps en ms]`, de 3s par défaut).

Le script est parallélisé : il crée par défaut un fil d'exécution par cœur, et chaque fil lance le nombre d'exécutions à faire divisé par le nombre de cœurs (option `-M` pour désactiver le parallélisme).

## Installation
### Dépendances
Pour avoir une barre de progression, il faut installer `tqdm` :
```
pip install tqdm
```

Il n'est pas nécessaire de l'installer pour que le script fonctionne néanmoins (mais sans il n'y aura pas de barre de progression).

### Script
Il faut télécharger le fichier `get_stats.py`, et le rendre exécutable :
```bash
chmod u+x get_stats.py
```

Pour le lancer, il est soit possible d'utiliser directement python :
```
python3 get_stats.py ...
```
ou alors :
```
./get_stats.py ...
```

## Usage
```
$ ./get_stats.py -h
usage: ./get_stats.py [-h] [-v] [-n NUMBER] [-t TIMEOUT] [-d DELAY] [-s SEED] [-G] [-P] [-M] [-T NB_THREADS] [-V] exec level

Show statistics about your bomberman AI

positional arguments:
  exec                  The executable name
  level                 The level file name

options:
  -h, --help            show this help message and exit
  -v, --version         Show version and exit
  -n NUMBER, --number NUMBER
                        The number of samples to use. Default is 100
  -t TIMEOUT, --timeout TIMEOUT
                        The time to wait before killing the program (in ms). Default is 3s
  -d DELAY, --delay DELAY
                        Add a delay (in milliseconds) between each execution
  -s SEED, --seed SEED  If given, run the program only once with that seed for random
  -G, --no-gdb          Do not use gdb to change the seed
  -P, --no-progress-bar
                        Do not use the progress bar
  -M, --no-multi-threading
                        Do not use multi threading
  -T NB_THREADS, --nb-threads NB_THREADS
                        The number of threads to use. Default is the number of available cores (4 on this machine). This argument is ignored of -M is used
  -V, --verbose         Show the board of each game

Examples :
	./get_stats.py ./bomberman level0.map
	./get_stats.py ./bomberman level0.map -n 5000
	./get_stats.py ./bomberman level2.map -n 60 -G -d 1000
```

## License
This project is licensed under the GNU General Public License v3.0 - see the [LICENSE](LICENSE) file for details
